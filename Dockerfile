FROM openjdk:11

ARG PROFILE

ENV PROFILE_ENV=$PROFILE

COPY ./target/*.jar /home/app.jar

ENTRYPOINT ["java", "-jar","-DPROFILE=${PROFILE_ENV}", "/home/app.jar"]