/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.domain;

import br.edu.calc.plus.repo.UsuarioRepo;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

/**
 *
 * @author Ronny
 */
@DataJpaTest
@ActiveProfiles("test")
public class UsuarioTest {
    
    @Autowired
    private UsuarioRepo userRepo;
       
    private Usuario usr;
    
    public UsuarioTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    @DisplayName("Verificar quantidade usuários cadastrados - RF 3")
    public void testQuantidadeUsuariosCadastrados() {
        this.usr = new Usuario(1, "ze", "ze", "ze@ze.com", "1234", "jf", LocalDate.of(2017, 2, 13));
        userRepo.save(usr);
        
        this.usr =  new Usuario(null, "Daves Martins", "daves", "daves@daves", new BCryptPasswordEncoder().encode( "123456" ), "JF", 
                		LocalDate.now().minusYears(30));         
        userRepo.save(usr);
        
        int resultado = (int) userRepo.count();
                
        int esperado = 2;       
       
        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("Testa se login é valido")
    public void testaLoginInValido() {
        this.usr = new Usuario(1, "ze", "ze", "ze", "1234", "jf", LocalDate.of(2017, 2, 13));
                
        boolean resultado = this.usr.loginValida();

        boolean esperado = false;       

        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("Testa espaço no login")
    public void testaEspacoNoLogin() {
        this.usr = new Usuario(1, "ze", "ze", " ze", "1234", "jf", LocalDate.of(2017, 2, 13));
                
        boolean resultado = this.usr.loginValida();

        boolean esperado = false;       

        assertEquals(esperado, resultado);
    }
    
    @Test
    @DisplayName("Testa se login é valido")
    public void testaLoginValido() {
        this.usr = new Usuario(1, "ze", "zegustin", "ze@ze.com.br", "1234", "jf", LocalDate.of(2017, 2, 13));
                
        boolean resultado = this.usr.loginValida();

        boolean esperado = true;       

        assertEquals(esperado, resultado);
    }
     
}
