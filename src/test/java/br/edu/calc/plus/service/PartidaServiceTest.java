/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.service;

import br.edu.calc.plus.domain.Jogo;
import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import br.edu.calc.plus.repo.JogoRepo;
import br.edu.calc.plus.repo.PartidaRepo;
import br.edu.calc.plus.repo.UsuarioRepo;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author danie
 */
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class PartidaServiceTest {

    public PartidaServiceTest() {
    }

    @MockBean
    private PartidaRepo pDao;

    @MockBean
    private JogoRepo jDao;

    @MockBean
    private UsuarioRepo uDao;

    @MockBean
    private JogoService jogoService;

    @InjectMocks
    PartidaService partidaService;

    private Usuario user = new Usuario(1, "ze", "ze", "ze@ze.com", "1234", "jf", LocalDate.of(2017, 2, 13));
    private Partida p = new Partida(null, LocalDateTime.now(), 0, 0);

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {

    }

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        Mockito.when(uDao.getById(1)).thenReturn(user);

    }

    @AfterEach
    public void tearDown() {
        pDao.deleteAll();
    }

    @Test
    public void testIniciarPartida() {
        try {
            
            Partida resultado = partidaService.iniciarPartida(1);
            assertEquals(p, resultado);
        } catch (Exception e) {
            fail("Erro: ", e);
        }
    }
    
    @Test
    public void testUserJaCompetiu() {
        LocalDateTime dtIni = LocalDate.now().atStartOfDay();
	LocalDateTime dtFim = LocalDate.now().atTime(23, 59, 59);
        Mockito.when(pDao.getUsuarioCompetil(1,dtIni,dtFim)).thenReturn(2l);
        
        var resposta = partidaService.userJaCompetiuHoje(1);
        
        assertEquals(true,resposta);
    }

}
