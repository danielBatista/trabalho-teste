/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.controller;

import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import br.edu.calc.plus.repo.UsuarioRepo;
import br.edu.calc.plus.service.JogoService;
import br.edu.calc.plus.service.PartidaService;
import br.edu.calc.plus.service.UsuarioService;
import br.edu.calc.plus.util.LogadoUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.time.LocalDate;
import java.util.ArrayList;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
/**
 *
 * @author Ronny
 */

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
public class HomeControllerTest {    

    @MockBean
    UsuarioService usuarioService;
    
    @MockBean
    PartidaService partidaService;
    
    @MockBean
    JogoService jogoService;
        
    public HomeControllerTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {        
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {   
        MockitoAnnotations.initMocks(this);
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testaTotaldeUsuariosCadastrados() {       
      
        ArrayList<Usuario> lista = new ArrayList<>();
        lista.add(new Usuario(1, "ze", "ze", "ze@ze.com", "1234", "jf", LocalDate.of(2017, 2, 13)));     
        lista.add(new Usuario(null, "Daves Martins", "daves", "daves@daves", new BCryptPasswordEncoder().encode("123456"), "JF",
               LocalDate.now().minusYears(30)));            
        
        Mockito.when(usuarioService.getCountUsers()).thenReturn(Long.valueOf(lista.size()));        
        
        long resultado = usuarioService.getCountUsers();

        long esperado = 2;
    
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testaTotaldeErros() {                 
        Mockito.when(jogoService.getAllErros()).thenReturn("2");        
        
        String resultado = jogoService.getAllErros();

        String esperado = "2";
    
        assertEquals(esperado, resultado);
    }
    
    @Test
    public void testaTotaldeAcertos() {                 
        Mockito.when(jogoService.getAllAcertos()).thenReturn(Long.valueOf(2));        
        
        long resultado = jogoService.getAllAcertos();

        long esperado = 2;
    
        assertEquals(esperado, resultado);
    } 
}
