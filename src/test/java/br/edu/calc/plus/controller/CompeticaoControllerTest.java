/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.calc.plus.controller;

import br.edu.calc.plus.config.security.user.UserLogado;
import br.edu.calc.plus.domain.Partida;
import br.edu.calc.plus.domain.Usuario;
import br.edu.calc.plus.repo.PartidaRepo;
import br.edu.calc.plus.repo.UsuarioRepo;
import br.edu.calc.plus.service.JogoService;
import br.edu.calc.plus.service.PartidaService;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 *
 * @author danie
 */
@ActiveProfiles("test")
@SpringBootTest
@AutoConfigureMockMvc
public class CompeticaoControllerTest {

    public CompeticaoControllerTest() {
    }

    @Autowired
    private MockMvc request;

    @Autowired
    private UsuarioRepo userRepo;

    @Autowired
    PartidaRepo pRepo;

    @Autowired
    JogoService jogoService;

    @Autowired
    PartidaService partidaService;

    @BeforeAll
    public static void setUpClass() {

    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {

    }

    @AfterEach
    public void tearDown() {
        //userRepo.deleteAll();
        //pRepo.deleteAll();
    }

    @Test
    public void testPaginaNovaCompeticao() throws Exception {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_CLIENTE"));
        UserLogado ul = new UserLogado(1, "zezin", "ze@ze", "ze", "123456", grantedAuthorities);

        ResultActions r = request.perform(MockMvcRequestBuilders.get("/competicao")
                .with(SecurityMockMvcRequestPostProcessors.user(ul))
        );

        r.andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

//     @Test
//     public void testIniciarPartida() throws Exception {
//         Usuario u = userRepo.findByLogin("daves").get();
//         Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
//         grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_CLIENTE"));
//         UserLogado ul = new UserLogado(u.getId(), u.getNome(), u.getEmail(), u.getLogin(), u.getSenha(), grantedAuthorities);

//         ResultActions r = request.perform(MockMvcRequestBuilders.get("/competicao/new")
//                 .with(SecurityMockMvcRequestPostProcessors.user(ul))
//                 .with(SecurityMockMvcRequestPostProcessors.csrf())
//         );

//         r.andExpect(MockMvcResultMatchers.status().isOk())
//                 .andDo(MockMvcResultHandlers.print());
//     }

//     @Test
//     public void testIniciarSegundaPartidaDia() throws Exception {
//         Usuario u = userRepo.findByLogin("daves").get();
//         Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
//         grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_CLIENTE"));
//         UserLogado ul = new UserLogado(u.getId(), u.getNome(), u.getEmail(), u.getLogin(), u.getSenha(), grantedAuthorities);

//         ResultActions r = request.perform(MockMvcRequestBuilders.get("/competicao/new")
//                 .with(SecurityMockMvcRequestPostProcessors.user(ul))
//                 .with(SecurityMockMvcRequestPostProcessors.csrf())
//         );

//         r.andExpect(MockMvcResultMatchers.status().is(302))
//                 .andExpect(MockMvcResultMatchers.redirectedUrl("/competicao"))
//                 .andDo(MockMvcResultHandlers.print());
//         pRepo.deleteAll();

//     }

}
